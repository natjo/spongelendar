$(document).ready(function() {
    addLongPressListener();
    initDoors();
    setOnClickClosedDoor();
    setOnClickOpenDoor();
    warnIfNotDecember();
});

function addLongPressListener() {
    document.addEventListener('long-press', function(e) {
        e.preventDefault()
        if (confirm('Do you want to reset the calendar?')) {
            deleteCalendarState();
            initDoors();
        }
    });

}

function initDoors() {
    let calendarState = loadCalendarState();
    for (day in calendarState) {
        if (calendarState[day] == 'calendar-door-open') {
            openDoor(day);
        } else {
            closeDoor(day);
        }
    }
}

function openDoor(id) {
    let door = $(`#${id}`);
    door.removeClass('calendar-door-closed');
    door.addClass('calendar-door-open');
    door.addClass('door-open');
    setOnClickOpenDoor();
}

function closeDoor(id) {
    let door = $(`#${id}`);
    door.addClass('calendar-door-closed');
    door.removeClass('calendar-door-open');
    door.removeClass('door-open');
    setOnClickClosedDoor();
}

function getDayfromId(id) {
    var matches = id.match(/(\d+)$/);
    if (!matches) {
        throw "Some error happened, the given id to the function 'getDayfromId' was not of the form day_12";
    }
    return matches[0];
}

function setOnClickOpenDoor() {
    $('.calendar-door-open').click(function() {
        // The id should be the filename of the meme without the .jpg extension
        let imagePath = `images/memes/${this.id}.jpg`;
        openModalWithDelay(imagePath, 1000);
    });
}

function setOnClickClosedDoor() {
    $('.calendar-door-closed').click(function() {
        if (isDoorAllowedToOpen(this.id)) {
            openDoor(this.id);
            setCalendarStateopen(this.id);
            showImage(this.id);
        }
    });
}

function isDoorAllowedToOpen(id) {
    let day = getDayfromId(id);
    let today = new Date();
    let isDecember = today.getMonth() == 11;
    let isDayInPast = today.getDate() >= parseInt(day);
    return isDecember && isDayInPast;
}

function showImage(id) {
    // The id should be the filename of the meme without the .jpg extension
    let imagePath = `images/memes/${id}.jpg`;
    openModalWithDelay(imagePath, 1000);
}

function setCalendarStateopen(id) {
    let calendarState = loadCalendarState();
    calendarState[id] = 'calendar-door-open';
    saveCalendarState(calendarState);
}

function warnIfNotDecember() {
    let today = new Date();
    if (today.getMonth() != 11) {
        window.alert("It's not December yet! You have to be patient for a little longer.");
    }
}
