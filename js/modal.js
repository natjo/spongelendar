function openModalWithDelay(imagePath, delay) {
    // Get the modal and its image
    var modal = document.getElementById("imagePopup");
    var modalImg = document.getElementById("img01");

    // Set image
    modalImg.src = imagePath;

    delayExec(() => modal.style.display = "block", delay);

    modal.onclick = function() {
        modal.style.display = "none";
    }
}

var delayExec = (function() {
    var timer = 0;
    return function(callback, ms) {
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
    };
})();
